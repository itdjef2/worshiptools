﻿using System;
using System.IO;
using WorshipTools.Droid.Services;
using WorshipTools.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace WorshipTools.Droid.Services
{
    public class FileHelper : IFileHelper
	{
		public string GetLocalFilePath(string filename)
		{
			string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			return Path.Combine(path, filename);
		}
	}
}
