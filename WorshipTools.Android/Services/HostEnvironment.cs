﻿using Android.Content.PM;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections;
using System.Collections.Generic;
using WorshipTools.Droid.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(HostEnvironment))]
namespace WorshipTools.Droid.Services
{
    public class HostEnvironment : IHostingEnvironment
    {
       
        public HostEnvironment()
        {
            
            ContentRootPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var fp = new PhysicalFileProvider(ContentRootPath);
            ContentRootFileProvider = fp;
            //ApplicationName = new ApplicationInfo().LoadLabel(AppContext. packageManager);

        }
        public string EnvironmentName { get; set; }
        public string ApplicationName { get; set; }
        public string WebRootPath { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IFileProvider WebRootFileProvider { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string ContentRootPath { get; set; }
        public IFileProvider ContentRootFileProvider { get; set; }
    }
}
