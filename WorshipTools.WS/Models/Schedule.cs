﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorshipTools.WS.Models
{
    public class Schedule
    {
        [Key]
        public long ScheduleId { get; set; }
        [Required]
        public DateTime WorshipDate { get; set; }

        public ICollection<ScheduledSong> ScheduledSongs { get; set; }
    }
}
