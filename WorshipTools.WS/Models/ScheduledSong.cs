﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorshipTools.WS.Models
{
    public class ScheduledSong
    {
        [Key]
        public long ScheduledSongId { get; set; }

        public Schedule Schedule { get; set; }

        public Song Song { get; set; }

    }
}
