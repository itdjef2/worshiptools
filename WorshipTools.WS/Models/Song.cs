﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorshipTools.WS.Models
{
    public class Song
    {
        [Key]
        public long SongId { get; set; }
        [Required]
        public string Name { get; set; }
        
        public string AlternativeName { get; set; }
        [Required]
        public string Tone { get; set; }
        public int Transpose { get; set; } = 0;
        public string Comments { get; set; }

        public ICollection<ScheduledSong> ScheduledSongs {get;set;}
    }
}
