﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WorshipTools.WS.Data;
using WorshipTools.WS.Models;

namespace WorshipTools.WS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduledSongsController : ControllerBase
    {
        private readonly WorshipDataContext _context;

        public ScheduledSongsController(WorshipDataContext context)
        {
            _context = context;
        }

        // GET: api/ScheduledSongs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ScheduledSong>>> GetScheduledSongs()
        {
            return await _context.ScheduledSongs.ToListAsync();
        }

        // GET: api/ScheduledSongs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ScheduledSong>> GetScheduledSong(long id)
        {
            var scheduledSong = await _context.ScheduledSongs.FindAsync(id);

            if (scheduledSong == null)
            {
                return NotFound();
            }

            return scheduledSong;
        }

        // PUT: api/ScheduledSongs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutScheduledSong(long id, ScheduledSong scheduledSong)
        {
            if (id != scheduledSong.ScheduledSongId)
            {
                return BadRequest();
            }

            _context.Entry(scheduledSong).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScheduledSongExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ScheduledSongs
        [HttpPost]
        public async Task<ActionResult<ScheduledSong>> PostScheduledSong(ScheduledSong scheduledSong)
        {
            _context.ScheduledSongs.Add(scheduledSong);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetScheduledSong", new { id = scheduledSong.ScheduledSongId }, scheduledSong);
        }

        // DELETE: api/ScheduledSongs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ScheduledSong>> DeleteScheduledSong(long id)
        {
            var scheduledSong = await _context.ScheduledSongs.FindAsync(id);
            if (scheduledSong == null)
            {
                return NotFound();
            }

            _context.ScheduledSongs.Remove(scheduledSong);
            await _context.SaveChangesAsync();

            return scheduledSong;
        }

        private bool ScheduledSongExists(long id)
        {
            return _context.ScheduledSongs.Any(e => e.ScheduledSongId == id);
        }
    }
}
