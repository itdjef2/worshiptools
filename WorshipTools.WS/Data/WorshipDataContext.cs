﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorshipTools.WS.Models;

namespace WorshipTools.WS.Data
{
    public class WorshipDataContext : DbContext
    {
        public WorshipDataContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Song> Songs { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduledSong> ScheduledSongs { get; set; }
    }
}
