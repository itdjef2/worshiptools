﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WorshipTools.Services
{
    public class WebAPIServices : IWebAPIServices
    {
        private HttpClient _client;

        public WebAPIServices()
        {
            _client = new HttpClient();

        }
        public async Task<T> Get<T>(string url)
        {
            try
            {
            
                var response = await _client.GetAsync(url);
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine("Excepción {0}", e.Message);
                return JsonConvert.DeserializeObject<T>(e.Message);
            }
        }

        public async Task<T> Post<T>(T item)
        {
            try
            {
                _client.BaseAddress = new Uri("http://www.joan-marc.com/api/Songs");
                //var json = JsonConvert.SerializeObject(item);
                //var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                //var response = await _client.PostAsync(new Uri("http://www.joan-marc.com/api/Songs"), stringContent);

                //var resp = await response.Content.ReadAsStringAsync();

                var response = await _client.PostAsJsonAsync("", item);

                return item;
            }
            catch (Exception e)
            {
                Console.WriteLine("Excepción {0}", e.Message);
                return JsonConvert.DeserializeObject<T>(e.Message);
            }
        }

        public async Task PushSync<T>(List<T> listItems)
        {
            try
            {
                
                foreach (T item in listItems)
                {
                    var json = JsonConvert.SerializeObject(item);
                    HttpContent content = new StringContent(json);
                    var response = await _client.PostAsync("http://www.joan-marc.com/api/Songs/PushSync", content);

                    var resp = await response.Content.ReadAsStringAsync();
                }
                //return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine("Excepción {0}", e.Message);
                //return JsonConvert.DeserializeObject<T>(e.Message);
            }
        }
        public async Task<ObservableCollection<T>> PullSync<T>(DateTimeOffset lastSync)
        {
            try
            {

                    var response = await _client.GetAsync("http://www.joan-marc.com/api/Songs/PullSync?since=" + lastSync.ToString());

                    var resp = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ObservableCollection<T>>(resp);
            }
            catch (Exception e)
            {
                Console.WriteLine("Excepción {0}", e.Message);
                return null;
                //return JsonConvert.DeserializeObject<T>(e.Message);
            }
        }
    }
}

