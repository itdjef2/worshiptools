﻿namespace WorshipTools.Services
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
