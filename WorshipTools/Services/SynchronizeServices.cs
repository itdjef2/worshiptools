﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using WorshipTools.Models;

namespace WorshipTools.Services
{
    public class SynchronizeServices : ISynchronizeServices
    {
        private IDataRepository dataRepository;
        private IWebAPIServices webAPI;
        public SynchronizeServices(IDataRepository dataRep, IWebAPIServices webAPISrv)
        {
            dataRepository = dataRep;
            webAPI = webAPISrv;
        }

        public async void Sync()
        {
            Settings st = await dataRepository.GetSettingsAsync();

            DateTimeOffset lastSync = st.LastSync;

            ObservableCollection<Song> songs = await dataRepository.GetSongListAsync();

            ObservableCollection<Song> changed = new ObservableCollection<Song>(songs.Where(x => x.LastUpdated >= lastSync || 
                                          (x.DeletedOn != null && x.DeletedOn >= lastSync)).ToList());

            await webAPI.PushSync<Song>(changed.ToList());

            ObservableCollection<Song> listSong = await webAPI.PullSync<Song>(lastSync);

            foreach (Song row in listSong)
                if (!songs.Any(x => x.SongId == row.SongId)) // Does not exist, hence insert 
                    await dataRepository.SaveSongAsync(row);
                else if (row.DeletedOn != null)
                    await dataRepository.DeleteSongAsync(row);
                else
                    await dataRepository.UpdateSongAsync(row);

            await dataRepository.SetLastSyncAsync();
        }
    }
}
