﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace WorshipTools.Services
{
    public interface IWebAPIServices
    {
        Task<T> Get<T>(string url);
        Task PushSync<T>(List<T> listItems);
        Task<ObservableCollection<T>> PullSync<T>(DateTimeOffset lastSync);
    }
}

