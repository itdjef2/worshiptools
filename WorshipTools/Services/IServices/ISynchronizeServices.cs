﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using WorshipTools.Models;

namespace WorshipTools.Services
{
    public interface ISynchronizeServices
    {
        void Sync();
    }
}
