﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WorshipTools.Models;

namespace WorshipTools.Services
{
    public interface IDataRepository
    {
        Task<int> SaveSettingsAsync(Settings item);
        Task<Settings> GetSettingsAsync();
        Task<int> SetLastSyncAsync();
        Task<ObservableCollection<Song>> GetSongListAsync();
        Task<Song> GetSongAsync(Guid id);
        Task<int> SaveSongAsync(Song item);
        Task<int> DeleteSongAsync(Song item);
        Task<int> UpdateSongAsync(Song item);
    }
}
