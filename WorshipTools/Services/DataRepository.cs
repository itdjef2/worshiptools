﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WorshipTools.DB;
using WorshipTools.Models;

namespace WorshipTools.Services
{
    public class DataRepository : IDataRepository
    {
        WorshipDBContext dbContext; 

        public DataRepository(WorshipDBContext dbCtx)
        {
            dbContext = dbCtx;
        }

        
        public async Task<Settings> GetSettingsAsync()
        {
            return await dbContext.Settings.SingleAsync(item => item.SettingsId == 1);
        }
        public async Task<int> SaveSettingsAsync(Settings item)
        {
            if (item.SettingsId == 0)
            {
                item.SettingsId = 1;
                await dbContext.Settings.AddAsync(item);
            }
            return await dbContext.SaveChangesAsync();
        }
        public async Task<int> SetLastSyncAsync()
        {
            Settings st = await GetSettingsAsync();
            st.LastSync = DateTimeOffset.Now;
            dbContext.Settings.Update(st);
            return await dbContext.SaveChangesAsync();
        }
        public async Task<ObservableCollection<Song>> GetSongListAsync()
        {
            return new ObservableCollection<Song>(await dbContext.Songs.ToListAsync());
        }

        public async Task<Song> GetSongAsync(Guid id)
        {
            return await dbContext.Songs.SingleAsync(item => item.SongId == id);
        }

        public async Task<int> SaveSongAsync(Song item)
        {
            if (item.SongId == Guid.Empty)
            {
                await dbContext.Songs.AddAsync(item);
            }
            return await dbContext.SaveChangesAsync();
        }

        public async Task<int> DeleteSongAsync(Song item)
        {
            dbContext.Songs.Remove(item);
            return await dbContext.SaveChangesAsync();
        }

        public async Task<int> UpdateSongAsync(Song item)
        {
            dbContext.Songs.Update(item);
            return await dbContext.SaveChangesAsync();
        }

    }
}
    