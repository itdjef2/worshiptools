﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using WorshipTools.Helpers;
using WorshipTools.Languages;
using Xamarin.Forms;

namespace WorshipTools.Services
{
    public class TranslationService
    {
        public event EventHandler OnLanguageChange = null;

        private static readonly List<string> availableLanguageCodes = new List<string>() { "es-CA", "es-ES" };
        private static readonly string defaultLanguage = "es-CA";
        private static readonly string defaultWSLanguage = "es";
       
        //Gets the native language Name, removing the country and capitalizing (ex. español (España) --> Español)
        public List<string> AvailableLanguages => availableLanguageCodes.Select(x => CultureInfo.GetCultureInfo(x).NativeName.Split('(')[0].Trim().Capitalize()).ToList();

        private string currentLanguage;

        public int CurrentLanguageIndex { get => AvailableLanguages.IndexOf(currentLanguage); }
        public string DefaultWSLanguage { get => defaultWSLanguage; }

        public TranslationService()
        {
            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                if (!availableLanguageCodes.Exists(s => s.Equals(ci.Name)))
                {
                    string lcAux = availableLanguageCodes.Find(s => s.Substring(0, 2).Equals(ci.Name.Substring(0, 2)));
                    if (lcAux == null || lcAux == string.Empty)
                        ci = CultureInfo.GetCultureInfo(defaultLanguage);
                    else
                        ci = CultureInfo.GetCultureInfo(lcAux);
                }
 
                LangResource.Culture = ci;
                DependencyService.Get<ILocalize>().SetLocale(ci);
            }
            currentLanguage = CultureInfo.GetCultureInfo(LangResource.Culture.Name).NativeName.Split('(')[0].Trim().Capitalize();
        }

        public void SetLanguage(string language)
        {
            LangResource.Culture = new CultureInfo(availableLanguageCodes[AvailableLanguages.IndexOf(language)]);

            currentLanguage = language;
            OnLanguageChange?.Invoke(language, EventArgs.Empty);
        }
    }
}
