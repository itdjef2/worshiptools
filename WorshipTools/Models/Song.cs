﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WorshipTools.Models
{
    public class Song
    {
        [Key]
        public Guid SongId { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public string AlternativeName { get; set; }
        [Required]
        public string Tone { get; set; }
        [Required]
        public int Transpose { get; set; } = 0;
        [Required]
        public string Comments { get; set; } = string.Empty;

        public DateTimeOffset? DeletedOn { get; set; }
        [Required]
        public DateTimeOffset? LastUpdated { get; set; } = DateTimeOffset.MinValue;
        public DateTimeOffset? ClientLastUpdated { get; set; }

        [Ignore]
        public int ClusteredIndex { get; set; }

        public ICollection<ScheduledSong> ScheduledSongs { get; set; }
    }
}
