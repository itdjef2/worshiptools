﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WorshipTools.Models
{
    public class Settings
    {
        [Key]
        public int SettingsId { get; set; }
        [Required]
        public DateTimeOffset LastSync { get; set; }

    }
}
