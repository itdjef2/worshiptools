﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WorshipTools.Models
{
    public class Log
    {
        [Key]
        public int LogID { get; set; }
        [Required]
        public DateTimeOffset TimeStamp { get; set; }
        [Required]
        public string FuncName { get; set; }
        [Required]
        public string Detail { get; set; }

    }
}
