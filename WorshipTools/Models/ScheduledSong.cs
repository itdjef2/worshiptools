﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorshipTools.Models
{
    public class ScheduledSong
    {
        [Key]
        public Guid ScheduledSongId { get; set; }

        [Required]
        public Schedule Schedule { get; set; }

        [Required]
        public Song Song { get; set; }

        public DateTimeOffset? DeletedOn { get; set; }
        [Required]
        public DateTimeOffset? LastUpdated { get; set; } = DateTimeOffset.MinValue;
        public DateTimeOffset? ClientLastUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusteredIndex { get; set; }

    }
}
