﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorshipTools.Models
{
    public class Schedule
    {
        [Key]
        public Guid ScheduleId { get; set; }
        [Required]
        public DateTime WorshipDate { get; set; }

        public DateTimeOffset? DeletedOn { get; set; }
        [Required]
        public DateTimeOffset? LastUpdated { get; set; } = DateTimeOffset.MinValue;
        public DateTimeOffset? ClientLastUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusteredIndex { get; set; }

        public ICollection<ScheduledSong> ScheduledSongs { get; set; }
    }
}
