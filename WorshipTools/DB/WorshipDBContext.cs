﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using WorshipTools.Models;

namespace WorshipTools.DB
{
    public class WorshipDBContext : DbContext
    {
        private readonly IHostingEnvironment _env;

        public WorshipDBContext(IHostingEnvironment env) : base()
        {
            _env = env;
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Song> Songs { get; set; }
        public DbSet<Settings> Settings { get; set; }


        #region Private implementation            

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbName = "WorshipToolsSQLite.db";
            var path = System.IO.Path.Combine(_env.ContentRootPath, dbName);          
            optionsBuilder.UseSqlite($"Filename={path};");
        }

        #endregion
    }
}
