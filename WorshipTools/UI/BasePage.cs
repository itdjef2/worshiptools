﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WorshipTools.UI
{
    public class BasePage : ContentPage
    {
        public class BasePageModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged = null;

            public BasePage Parent = null;

            private Rectangle alertsSize = new Rectangle(0, 0, 1, 0);
            public Rectangle AlertsSize
            {
                get => alertsSize;
                set
                {
                    alertsSize = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AlertsSize)));
                }
            }

            public ObservableCollection<Alert> ActiveAlerts { get; set; } = new ObservableCollection<Alert>();
        }

        public enum AlertColor { Red, Green, Yellow };

        public event EventHandler OnPopped = null;

        /// <summary>
        /// It anounces that page is being removed from stack instead of popped
        /// </summary>
        protected bool IsRemoved { get; set; } = false;

        protected bool ShowSendOperationsPopped { get; set; } = false;

        public BasePageModel VMContent { get; set; } = new BasePageModel();
        private Alert alertSynchro = null;

        protected readonly Services.TranslationService translationSrv = null;
        
        public BasePage()
        {
            translationSrv = App.ServiceProvider.GetRequiredService<Services.TranslationService>();
            
            VMContent.Parent = this;
            BindingContext = VMContent;
        }

        public virtual Task InitializeUI()
        {
            BackgroundColor = Color.White;

            return Task.CompletedTask;
        }

        public virtual void Translate() { }

        public virtual async Task OnPush()
        {
            translationSrv.OnLanguageChange += TranslationSrv_OnLanguageChange;

            await InitializeUI();
        }

        
        
        
        private void TranslationSrv_OnLanguageChange(object sender, EventArgs e) => Translate();

        public virtual Task OnPop()
        {
            translationSrv.OnLanguageChange -= TranslationSrv_OnLanguageChange;

          
            OnPopped?.Invoke(this, EventArgs.Empty);

            return Task.CompletedTask;
            
        }

        public async Task RemovePages(params Type[] types)
        {
            foreach (var pageType in types)
            {
                var page = Navigation.NavigationStack.FirstOrDefault(p => p.GetType() == pageType);
                if (page is BasePage)
                    await ((BasePage)page).RemovePage();
                else if (page != null)
                    Navigation.RemovePage(page);

                if (page != null)
                    await Task.Delay(100); //https://github.com/xamarin/Xamarin.Forms/blob/master/Xamarin.Forms.Platform.Android/Renderers/ScrollViewRenderer.cs OnScrollToRequested internal fail without delay if page removed has a listview
            }
        }

        public async Task RemovePage()
        {
            IsRemoved = true;

            //force OnPop and OnDisappearing because we delete directly
            OnDisappearing();
            await OnPop();
            Navigation.RemovePage(this);
        }

        protected Alert NewAlert(string message1, Action action, string message2 = null, AlertColor color = AlertColor.Yellow)
        {
            string backgroundColor = color == AlertColor.Yellow ? "#fdca45" : color == AlertColor.Red ? "#f42837" : "#4ca92b";
            string textColor = color == AlertColor.Yellow ? "Black" : "White";
            var alert = new Alert() { Message1 = message1, Message2 = message2, OnClick = new Command(action), BackgroundColor = backgroundColor, TextColor = textColor };
            VMContent.ActiveAlerts.Add(alert);

            VMContent.AlertsSize = new Rectangle(0, 0, 1, VMContent.ActiveAlerts.Count * 48);

            return alert;
        }

        protected void RemoveAlert(Alert alert)
        {
            if (alert != null)
                VMContent.ActiveAlerts.Remove(alert);

            VMContent.AlertsSize = new Rectangle(0, 0, 1, VMContent.ActiveAlerts.Count * 48);
        }

        public class Alert
        {
            public string Message1 { get; set; }
            public string Message2 { get; set; }
            public bool Message2Visible { get => Message2?.Length > 0; }
            public Command OnClick { get; set; }
            public string BackgroundColor { get; set; }
            public string TextColor { get; set; }
        }
    }
}
