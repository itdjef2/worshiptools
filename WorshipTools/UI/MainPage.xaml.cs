﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorshipTools.UI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void BtnNewSong_Clicked(object sender, EventArgs e)
        {
            await navigation.PushAsync(ServiceProvider.GetRequiredService<MainPage>());

        }
    }
}