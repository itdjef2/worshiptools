﻿using Microsoft.Extensions.DependencyInjection;
using System;
using WorshipTools.Models;
using WorshipTools.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorshipTools.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SongListView : ContentPage
    {
        public SongListView(IServiceProvider srvProvider)
        {
            InitializeComponent();
            btnCanco.Text = Languages.LangResource.Canco;
            BindingContext = srvProvider.GetRequiredService<SongListViewModel>();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}