﻿using System;
using System.ComponentModel;
using WorshipTools.DB;
using WorshipTools.Models;
using WorshipTools.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorshipTools.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SongEditView : ContentPage, INotifyPropertyChanged
    {
        private readonly IDataRepository dRepo;
        public SongEditView(IDataRepository dataRepo)
        {
            dRepo = dataRepo;
            InitializeComponent();
        }

        async void OnSaveClicked(object sender, EventArgs e)
        {
            Song item = (Song)BindingContext;
            await dRepo.SaveSongAsync(item);
            await Navigation.PopAsync();
        }

        async void OnDeleteClicked(object sender, EventArgs e)
        {
            Song item = (Song)BindingContext;
            await dRepo.DeleteSongAsync(item);
            await Navigation.PopAsync();
        }

        async void OnCancelClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}