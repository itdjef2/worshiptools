﻿using System.Globalization;

namespace WorshipTools.Languages
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
        void SetLocale(CultureInfo ci);
    }
}
