﻿using System.Net.Mail;

namespace WorshipTools.Helpers
{
    public static class Extensions
    {
        
        public static string Capitalize(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            return text[0].ToString().ToUpper() + (text.Length > 1 ? text.Substring(1).ToLower() : null);
        }

        public static bool IsValidEmail(this string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch { return false; }
        }
        
    }
}
