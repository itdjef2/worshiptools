﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WorshipTools.DB;
using WorshipTools.UI;
using WorshipTools.UI.Views;
using WorshipTools.Services;
using Microsoft.AspNetCore.Hosting;
using WorshipTools.ViewModels;
using WorshipTools.Models;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace WorshipTools
{
    public partial class App : Application
    {
        public static ServiceCollection services = new ServiceCollection();
        public static IServiceProvider ServiceProvider { get; set; }

        public App()
        {
            InitializeComponent();
            RegisterServices();
            ShowMainPage();
        }

        public void RegisterServices()
        {
            services.AddSingleton<TranslationService>();
            services.AddSingleton(DependencyService.Get<IHostingEnvironment>());
            services.AddEntityFrameworkSqlite();
            services.AddDbContext<WorshipDBContext>();
            services.AddSingleton<IDataRepository, DataRepository>();
            services.AddSingleton<IWebAPIServices, WebAPIServices>();
            services.AddSingleton<ISynchronizeServices, SynchronizeServices>();
            
            //Models
            services.AddTransient<SongListViewModel>();

            //Views
            services.AddTransient<MainPage>();
            services.AddTransient<SongListView>();
            services.AddTransient<SongEditView>();

            ServiceProvider = services.BuildServiceProvider();

            ISynchronizeServices SyncSrv = ServiceProvider.GetRequiredService<ISynchronizeServices>();

            //SyncSrv.Sync();
            ///CreateBDD();

            //NewSong();

        }

        private async void CreateBDD()
        {
            WorshipDBContext db = ServiceProvider.GetRequiredService<WorshipDBContext>();
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            IDataRepository dr = ServiceProvider.GetRequiredService<IDataRepository>();
            await dr.SaveSettingsAsync(new Settings() { LastSync = DateTimeOffset.MinValue });
            //db.Database.Migrate();
        }
        private async void ShowMainPage()
        {
            NavigationPage navigation = new NavigationPage();

            navigation.BarBackgroundColor = (Color)Application.Current.Resources["primaryGreen"];
            navigation.BarTextColor = Color.White;

            MainPage = navigation;

            await navigation.PushAsync(ServiceProvider.GetRequiredService<MainPage>());

        }

        private void NewSong()
        {
            Song song = new Song();

            song.SongId = Guid.NewGuid();
            song.AlternativeName = "Beneiré";
            song.Comments = "ccc";
            song.Name = "10000 raons";
            song.Tone = "D";
            song.Transpose = 0;

            WebAPIServices was = new WebAPIServices();

            var aa = was.Post(song);

        }
        protected override void OnStart()
        {
            //Debug.WriteLine("OnStart");

            //// always re-set when the app starts
            //// users expect this (usually)
            ////			Properties ["ResumeAtWorshipToolsId"] = "";
            //if (Properties.ContainsKey("ResumeAtWorshipToolsId"))
            //{
            //	var rati = Properties["ResumeAtWorshipToolsId"].ToString();
            //	Debug.WriteLine("   rati=" + rati);
            //	if (!String.IsNullOrEmpty(rati))
            //	{
            //		Debug.WriteLine("   rati=" + rati);
            //		ResumeAtWorshipToolsId = int.Parse(rati);

            //		if (ResumeAtWorshipToolsId >= 0)
            //		{
            //			var WorshipToolsPage = new WorshipToolsItemPage();
            //			WorshipToolsPage.BindingContext = await Database.GetItemAsync(ResumeAtWorshipToolsId);
            //			await MainPage.Navigation.PushAsync(WorshipToolsPage, false); // no animation
            //		}
            //	}
            //}
        }

        protected override void OnSleep()
        {
            //Debug.WriteLine("OnSleep saving ResumeAtWorshipToolsId = " + ResumeAtWorshipToolsId);
            //// the app should keep updating this value, to
            //// keep the "state" in case of a sleep/resume
            //Properties["ResumeAtWorshipToolsId"] = ResumeAtWorshipToolsId;
        }

        protected override void OnResume()
        {
            //Debug.WriteLine("OnResume");
            //if (Properties.ContainsKey("ResumeAtWorshipToolsId"))
            //{
            //	var rati = Properties["ResumeAtWorshipToolsId"].ToString();
            //	Debug.WriteLine("   rati=" + rati);
            //	if (!String.IsNullOrEmpty(rati))
            //	{
            //		Debug.WriteLine("   rati=" + rati);
            //		ResumeAtWorshipToolsId = int.Parse(rati);

            //		if (ResumeAtWorshipToolsId >= 0)
            //		{
            //			var WorshipToolsPage = new WorshipToolsItemPage();
            //			WorshipToolsPage.BindingContext = await Database.GetItemAsync(ResumeAtWorshipToolsId);
            //			await MainPage.Navigation.PushAsync(WorshipToolsPage, false); // no animation
            //		}
            //	}
            //}
        }
    }
}

