﻿using System.Collections.ObjectModel;
using WorshipTools.Models;
using WorshipTools.Services;
using Xamarin.Forms;

namespace WorshipTools.ViewModels
{
    class SongListViewModel : ViewModelBase
    {
        private Song _songs;

        private IDataRepository _dataRepository;

        public SongListViewModel(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;

            FetchContacts();
        }

        private async void FetchContacts()
        {
            SongList = await _dataRepository.GetSongListAsync();
            Settings st = await _dataRepository.GetSettingsAsync();
        }
        public string Name
        {
            get => _songs.Name;
            set
            {
                _songs.Name = value;
                OnPropertyChanged();
            } 
        }

        private ObservableCollection<Song> _songList;

        public ObservableCollection<Song> SongList
        {
            get => _songList;
            set
            {
                _songList = value;
                OnPropertyChanged();
            }
        }
    }
}
